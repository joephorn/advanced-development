//PORTFOLIO animatie

var timeline = anime.timeline({
    loop: false,
    duration: 6000,
    easing: 'easeInOutQuad'
});

timeline.add({
    targets: '#svg path',
    strokeDashoffset: [anime.setDashoffset, 0],
    duration: 5000
})
.add({
    targets: '#svg path',
    fill: '#d8ad2f',
    duration: 3000
})

//onload animaties
var itemArr = document.getElementsByClassName('onloaditem');

function animateItems(translateX, translateY, i){
    anime({
      targets: itemArr[i],
      translateX,
      translateY,
      duration: 1000,
      opacity: 1,
      easing: 'easeInOutQuad'
    });
}

itemArr[0].addEventListener('load', animateItems(0, 20, 0));
itemArr[1].addEventListener('load', animateItems(-250, 0, 1));
itemArr[2].addEventListener('load', animateItems(580, 0, 2));
itemArr[3].addEventListener('load', animateItems(0, -50, 3));

//aboutbtn loop
var test1 = document.getElementById('about');

anime({
    targets: test1,
    duration: 1000,
    loop: true,
    direction: 'alternate',
    scale: 1.1,
    easing: 'easeInOutQuad'
});

//leerdoel hover
var leerdoelArr = document.getElementsByClassName('leerdoel');

function animateText(translateX, duration, i){
    anime.remove(leerdoelArr[i]);
    anime({
      targets: leerdoelArr[i],
      translateX: translateX,
      duration: duration,
      elasticity: 400
    });
}

function enterText(i){
    animateText(200, 800, i);
}
  
function leaveText(i) {
    animateText(-200, 400, i);
}
  
for (var i = 0; i < leerdoelArr.length; i++){
    console.log(i);
    leerdoelArr[i].addEventListener('mouseenter', enterText(i));
    leerdoelArr[i].addEventListener('mouseleave', leaveText(i));
}